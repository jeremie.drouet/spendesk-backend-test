FROM node:lts-alpine AS tester

WORKDIR /code
COPY package.json /code/package.json
COPY package-lock.json /code/package-lock.json
RUN npm ci
COPY . /code
RUN npm run doc

ENTRYPOINT [ "npm", "test" ]

FROM node:lts-alpine

WORKDIR /code
COPY package.json /code/package.json
COPY package-lock.json /code/package-lock.json
RUN npm ci --production
COPY . /code
COPY --from=tester /code/apidoc /code/apidoc

ENTRYPOINT [ "npm", "start" ]
