# Spendesk backend test

This project is running with NodeJS and only requires docker and docker-compose installed to dev/run it.

## Build the project

```bash
docker-compose build
```

## Run the tests

```bash
docker-compose run tester
```

Under the hood, it will start postgres, wait for it to be ready and run the mocha tests.

## Run the project

```bash
docker-compose up -d server
```

Under the hood, it will start postgres, wait for it to be ready and start the server.

The you can access the documentation on http://localhost:3000
