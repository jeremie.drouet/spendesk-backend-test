const config = require('../src/config').database;

module.exports = {
  development: {
    url: config.url,
  },
  test: {
    url: config.url,
  },
  production: {
    url: config.url,
  },
};
