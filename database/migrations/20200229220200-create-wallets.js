module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('wallets', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      company_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      balance: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      master: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('wallets');
  },
};
