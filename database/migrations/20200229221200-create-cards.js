module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('cards', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      wallet_id: {
        allowNull: false,
        type: DataTypes.UUID,
        references: {
          model: 'wallets',
          key: 'id',
        },
      },
      user_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      balance: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      number: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      cvv: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      expiration: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      blocked: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('cards');
  },
};
