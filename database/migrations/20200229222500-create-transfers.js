module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('transfers', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      amount: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      origin_currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      target_currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      conversion_fee: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      origin_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      origin_type: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      target_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      target_type: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('transfers');
  },
};
