const { expect } = require('chai');
const nock = require('nock');
const models = require('../src/model');
const Wallet = require('./helper/wallet');
const request = require('./helper/server');

describe('wallet transfer endpoint', function() {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should work with same currency', async function() {
    const origin = await Wallet.create({ balance: 1000, currency: 'USD' });
    const target = await Wallet.create({ balance: 1000, currency: 'USD' });
    return request
      .post('/api/transfers')
      .send({
        origin_id: origin.id,
        target_id: target.id,
        amount: 100,
      })
      .expect(200)
      .then(() => models.Wallet.findByPk(origin.id))
      .then((updatedOrigin) => {
        expect(updatedOrigin).to.have.property('balance', 900);
      })
      .then(() => models.Wallet.findByPk(target.id))
      .then((updatedTarget) => {
        expect(updatedTarget).to.have.property('balance', 1100);
      });
  });

  it('should work with difference currencies', async function() {
    const origin = await Wallet.create({ balance: 1000, currency: 'USD' });
    const target = await Wallet.create({ balance: 1000, currency: 'EUR' });
    const scope = nock('http://data.fixer.io')
      .get('/api/latest')
      .query({
        access_key: '93674d960bc8b997d469c4b63c639236',
        base: 'USD',
        symbols: 'EUR',
      })
      .reply(200, {
        success: true,
        timestamp: Date.now(),
        base: 'USD',
        date: new Date().toString(),
        rates: {
          EUR: 0.813399,
        },
      });
    return request
      .post('/api/transfers')
      .send({
        origin_id: origin.id,
        target_id: target.id,
        amount: 100,
      })
      .expect(200)
      .then(() => models.Wallet.findByPk(origin.id))
      .then((updatedOrigin) => {
        expect(updatedOrigin).to.have.property('balance', 900);
      })
      .then(() => models.Wallet.findByPk(target.id))
      .then((updatedTarget) => {
        expect(updatedTarget).to.have.property('balance', 1079);
      })
      .then(() => {
        scope.done();
      });
  });
});
