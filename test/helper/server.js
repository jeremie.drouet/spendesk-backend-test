const supertest = require('supertest');
const server = require('../../src/server');

module.exports = supertest(server);
