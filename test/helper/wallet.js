const faker = require('faker');
const models = require('../../src/model');

const create = (override = {}) => models.Wallet.create({
  company_id: faker.random.uuid(),
  currency: 'USD',
  ...override,
});

module.exports = {
  create,
};
