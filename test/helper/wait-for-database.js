const { wait } = require('./time');
const sequelize = require('../../src/service/database');

const check = () => sequelize.authenticate();

const iterate = (retry = 100) => {
  console.log('trying to connect retry =', retry);
  return check().catch(async (err) => {
    console.error(err.message);
    if (retry <= 0) throw err;
    await wait(2500);
    return iterate(retry - 1);
  });
};

const run = () => iterate().then(() => sequelize.close());

module.exports = {
  check,
  run,
};

if (!module.parent) {
  run()
    .then(() => {
      console.log('connected to database');
      process.exit(0);
    })
    .catch((err) => {
      console.error(err.message);
      process.exit(1);
    });
}
