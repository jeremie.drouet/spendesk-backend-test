const faker = require('faker');
const models = require('../../src/model');

const create = (wallet_id, override = {}) => models.Card.create({
  user_id: faker.random.uuid(),
  currency: 'USD',
  ...override,
  wallet_id,
});

module.exports = {
  create,
};
