const { expect } = require('chai');
const faker = require('faker');
const request = require('./helper/server');
const Card = require('./helper/card');
const Wallet = require('./helper/wallet');

describe('wallet endpoint', function() {
  it('should list all the wallets for a user', async function() {
    const user_id = faker.random.uuid();
    const other_user_id = faker.random.uuid();
    const wallet_a = await Wallet.create();
    await Card.create(wallet_a.id, { user_id });
    const wallet_b = await Wallet.create();
    await Card.create(wallet_b.id, { user_id });
    const wallet_c = await Wallet.create();
    await Card.create(wallet_c.id, { user_id: other_user_id });
    return request
      .get('/api/wallets')
      .query({ user_id })
      .expect(200)
      .expect((res) => {
        expect(res.body)
          .to.be.an('array')
          .to.have.length(2);
      });
  });

  it('should return nothing if the user does not exist', function() {
    return request
      .get('/api/wallets')
      .query({ user_id: faker.random.uuid() })
      .expect(200)
      .expect((res) => {
        expect(res.body)
          .to.be.an('array')
          .to.have.length(0);
      });
  });
});
