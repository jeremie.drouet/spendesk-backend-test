const { expect } = require('chai');
const faker = require('faker');
const request = require('./helper/server');

describe('wallet endpoint', function() {
  it('should allow user to create a wallet', function() {
    const company_id = faker.random.uuid();
    return request
      .post('/api/wallets')
      .send({
        company_id,
        currency: 'USD',
      })
      .expect(200)
      .expect((res) => {
        expect(res.body)
          .to.have.property('id')
          .to.be.a('string');
        expect(res.body).to.have.property('balance', 0);
        expect(res.body).to.have.property('currency', 'USD');
        expect(res.body).to.have.property('company_id', company_id);
        expect(res.body).to.have.property('master', false);
      });
  });

  it('should only accept defined currencies', function() {
    const company_id = faker.random.uuid();
    return request
      .post('/api/wallets')
      .send({
        company_id,
        currency: 'BTC',
      })
      .expect(400)
      .expect((res) => {
        expect(res.body).to.have.property('name', 'ValidationError');
        expect(res.body).to.have.property('details');
      });
  });

  it('should take a company_id', function() {
    return request
      .post('/api/wallets')
      .send({
        currency: 'EUR',
      })
      .expect(400)
      .expect((res) => {
        expect(res.body).to.have.property('name', 'ValidationError');
        expect(res.body).to.have.property('details');
      });
  });
});
