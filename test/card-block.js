const { expect } = require('chai');
const request = require('./helper/server');
const models = require('../src/model');
const Card = require('./helper/card');
const Wallet = require('./helper/wallet');

describe('card block endpoint', function() {
  it('should transfer money from card to wallet', async function() {
    const wallet = await Wallet.create({ balance: 1000 }); // 10 USD
    const card = await Card.create(wallet.id, { balance: 1000 }); // 10 USD
    return request
      .post(`/api/cards/${card.id}/block`)
      .expect(200)
      .expect((res) => {
        expect(res.body).to.have.property('blocked', true);
        expect(res.body).to.have.property('balance', 0);
      })
      .then(() => models.Wallet.findByPk(wallet.id))
      .then((updated) => {
        expect(updated.balance, 2000);
      })
      .then(() => request.post(`/api/cards/${card.id}/unblock`).expect(200));
  });
});
