const { expect } = require('chai');
const faker = require('faker');
const models = require('../src/model');
const request = require('./helper/server');

describe('card endpoint', function() {
  it('should allow user to create a wallet', function() {
    const company_id = faker.random.uuid();
    const user_id = faker.random.uuid();
    return models.Wallet.create({
      company_id,
      currency: 'USD',
    }).then((wallet) =>
      request
        .post(`/api/wallets/${wallet.id}/cards`)
        .send({
          user_id,
          currency: 'USD',
        })
        .expect(200)
        .expect((res) => {
          expect(res.body)
            .to.have.property('id')
            .to.be.a('string');
          expect(res.body).to.have.property('wallet_id', wallet.id);
          expect(res.body).to.have.property('balance', 0);
          expect(res.body).to.have.property('currency', 'USD');
          expect(res.body).to.have.property('user_id', user_id);
          expect(res.body).to.have.property('expiration');
          expect(res.body).to.have.property('number').to.match(/^[0-9]{16}$/);
          expect(res.body).to.have.property('cvv').to.match(/^[0-9]{3}$/);
          expect(res.body).to.have.property('blocked', false);
        }),
    );
  });

  it('should only accept defined currencies', function() {
    const company_id = faker.random.uuid();
    const user_id = faker.random.uuid();
    return models.Wallet.create({
      company_id,
      currency: 'USD',
    }).then((wallet) =>
      request
        .post(`/api/wallets/${wallet.id}/cards`)
        .send({
          user_id,
          currency: 'BTC',
        })
        .expect(400)
        .expect((res) => {
          expect(res.body).to.have.property('name', 'ValidationError');
          expect(res.body).to.have.property('details');
        }),
    );
  });

  it('should fail if wallet does not exist', function() {
    const wallet_id = faker.random.uuid();
    const user_id = faker.random.uuid();
    return request
      .post(`/api/wallets/${wallet_id}/cards`)
      .send({
        user_id,
        currency: 'USD',
      })
      .expect(412);
  });
});
