const path = require('path');
const Sequelize = require('sequelize');
const Umzug = require('umzug');
const sequelize = require('../src/service/database');

const resetMigration = () => {
  const umzug = new Umzug({
    storage: 'sequelize',
    storageOptions: { sequelize },
    migrations: {
      params: [sequelize.getQueryInterface(), Sequelize],
      path: path.join(__dirname, '../database/migrations'),
    },
  });
  return umzug.down({ to: 0 }).then(() => umzug.up());
};

beforeEach(function() {
  this.timeout(2000);
  return resetMigration();
});

after(function() {
  return sequelize.close();
});
