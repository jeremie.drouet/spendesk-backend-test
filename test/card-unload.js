const { expect } = require('chai');
const faker = require('faker');
const request = require('./helper/server');
const Card = require('./helper/card');
const Wallet = require('./helper/wallet');

describe('card endpoint', function() {
  it('should transfer money from card to wallet', async function() {
    const wallet = await Wallet.create({ balance: 0 }); // 0 USD
    const card = await Card.create(wallet.id, { balance: 1200 }); // 12 USD
    return request
      .post(`/api/cards/${card.id}/unload`)
      .send({ amount: 500 }) // 5 USD
      .expect(200)
      .expect((res) => {
        expect(res.body).to.have.property('wallet_id', wallet.id);
        expect(res.body).to.have.property('balance', 700);
      });
  });

  it('should not transfer money if not enough fund', async function() {
    const wallet = await Wallet.create({ balance: 0 }); // 0 USD
    const card = await Card.create(wallet.id, { balance: 0 }); // 0 USD
    return request
      .post(`/api/cards/${card.id}/unload`)
      .send({ amount: 500 }) // 5 USD
      .expect(412);
  });
});
