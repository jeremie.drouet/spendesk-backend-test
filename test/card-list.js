const { expect } = require('chai');
const faker = require('faker');
const request = require('./helper/server');
const Card = require('./helper/card');
const Wallet = require('./helper/wallet');

describe('card endpoint', function() {
  it('should list all the cards for a user', async function() {
    const user_id = faker.random.uuid();
    const other_user_id = faker.random.uuid();
    const wallet = await Wallet.create();
    await Card.create(wallet.id, { user_id });
    await Card.create(wallet.id, { user_id });
    await Card.create(wallet.id, { user_id: other_user_id });
    return request
      .get('/api/cards')
      .query({ user_id })
      .expect(200)
      .expect((res) => {
        expect(res.body)
          .to.be.an('array')
          .to.have.length(2);
        res.body.forEach((card) => {
          expect(card).to.have.property('wallet_id', wallet.id);
          expect(card).to.have.property('balance', 0);
          expect(card).to.have.property('currency', 'USD');
          expect(card).to.have.property('user_id', user_id);
          expect(card).to.have.property('expiration');
          expect(card)
            .to.have.property('number')
            .to.match(/^[0-9]{16}$/);
          expect(card)
            .to.have.property('cvv')
            .to.match(/^[0-9]{3}$/);
          expect(card).to.have.property('blocked', false);
        });
      });
  });

  it('should return nothing if the user does not exist', function() {
    return request
      .get('/api/cards')
      .query({ user_id: faker.random.uuid() })
      .expect(200)
      .expect((res) => {
        expect(res.body)
          .to.be.an('array')
          .to.have.length(0);
      });
  });
});
