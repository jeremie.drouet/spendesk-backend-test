const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const app = express();

app.use(morgan('tiny'));
app.use(bodyParser.json());

app.get('/api/cards', require('./controller/card/list'));
app.post('/api/cards/:card_id/block', require('./controller/card/block'));
app.post('/api/cards/:card_id/unblock', require('./controller/card/unblock'));
app.post('/api/cards/:card_id/load', require('./controller/card/load'));
app.post('/api/cards/:card_id/unload', require('./controller/card/unload'));
app.post('/api/transfers', require('./controller/transfer/create'));
app.post('/api/wallets/:wallet_id/cards', require('./controller/card/create'));
app.post('/api/wallets', require('./controller/wallet/create'));
app.get('/api/wallets', require('./controller/wallet/list'));
app.use(express.static('apidoc'));

app.use(require('./middleware/error-handler'));

module.exports = app;
