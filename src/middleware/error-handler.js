module.exports = (err, req, res, next) => {
  if (err.isJoi) {
    return res.status(400).json({
      name: 'ValidationError',
      details: err.details,
    });
  }
  if (err.isBoom) {
    return res.status(err.output.statusCode).json(err.output.payload);
  }
  console.log(err);
  return res.status(500).json(err);
};
