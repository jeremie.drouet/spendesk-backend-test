const createDigits = (len) => {
  return [...new Array(len)]
    .map(() => Math.floor(Math.random() * 9).toString())
    .join('');
};

module.exports = {
  createDigits,
};
