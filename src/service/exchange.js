const axios = require('axios');
const config = require('../config');

const getRate = (origin, target) => {
  if (origin === target) return 1.0;
  return axios
    .get('http://data.fixer.io/api/latest', {
      params: {
        access_key: config.fixer.accessKey,
        base: origin,
        symbols: target,
      },
    })
    .then((res) => res.data.rates[target]);
};

const getConversionFeeRate = (origin, target) => {
  if (origin === target) return 0.0;
  return 0.029;
};

module.exports = {
  getRate,
  getConversionFeeRate,
};
