const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {post} /api/cards/:card_id/unload Unload money from the card
 * @apiName LoadCard
 * @apiGroup Card
 *
 * @apiParams (params) card_id ID of the card
 *
 * @apiParams (body) amount Amount of money (cents) to load.
 *
 * @apiSuccess {String} id ID of the wallet
 * @apiSuccess {String} company_id id of the company related to the wallet
 * @apiSuccess {String=USD,GBP,EUR} currency currencu of the wallet
 * @apiSuccess {Number} balance number of cents on the wallet
 * @apiSuccess {Boolean} master if the wallet is the master wallet
 */

const paramsSchema = Joi.object()
  .keys({
    card_id: Joi.string()
      .uuid()
      .required(),
  })
  .required();

const bodySchema = Joi.object()
  .keys({
    amount: Joi.number()
      .positive()
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const params = paramsSchema.validate(req.params, { stripUnknown: true });
  if (params.error) return next(params.error);
  const body = bodySchema.validate(req.body, { stripUnknown: true });
  if (body.error) return next(body.error);
  return models.Transfer.unloadCard(params.value.card_id, Math.floor(body.value.amount))
    .then(() => models.Card.findByPk(params.value.card_id))
    .then((card) => res.json(card))
    .catch(next);
};
