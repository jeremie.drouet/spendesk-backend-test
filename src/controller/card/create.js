const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {post} /api/wallets/:wallet_id/cards Create a card
 * @apiName CreateCard
 * @apiGroup Card
 *
 * @apiParam (params) {String} wallet_id ID of the wallet
 *
 * @apiParam (body) {String} user_id ID of the user linked to the card
 * @apiParam (body) {String=USD,GBP,EUR} currency Currency of the card
 *
 * @apiSuccess {String} id ID of the wallet
 * @apiSuccess {String} company_id id of the company related to the wallet
 * @apiSuccess {String=USD,GBP,EUR} currency currencu of the wallet
 * @apiSuccess {Number} balance number of cents on the wallet
 * @apiSuccess {Boolean} master if the wallet is the master wallet
 *
 * @apiError ValidationError request body didn't satisfy requirements
 */

const paramsSchema = Joi.object()
  .keys({
    wallet_id: Joi.string()
      .uuid()
      .required(),
  })
  .required();

const bodySchema = Joi.object()
  .keys({
    user_id: Joi.string()
      .uuid()
      .required(),
    currency: Joi.string()
      .uppercase()
      .valid('USD', 'GBP', 'EUR')
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const params = paramsSchema.validate(req.params, { stripUnknown: true });
  if (params.error) return next(params.error);
  const body = bodySchema.validate(req.body, { stripUnknown: true });
  if (body.error) return next(body.error);
  return models.Card.create({
    ...params.value,
    ...body.value,
  })
    .then((wallet) => res.json(wallet))
    .catch((err) => {
      if (err.name === 'SequelizeForeignKeyConstraintError') {
        throw Boom.preconditionFailed('specified wallet does not exist');
      }
      throw err;
    })
    .catch(next);
};
