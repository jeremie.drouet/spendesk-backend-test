const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {post} /api/cards/:card_id/block Block the card
 * @apiName BlockCard
 * @apiGroup Card
 *
 * @apiParams (params) card_id ID of the card
 */

const paramsSchema = Joi.object()
  .keys({
    card_id: Joi.string()
      .uuid()
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const params = paramsSchema.validate(req.params, { stripUnknown: true });
  if (params.error) return next(params.error);
  return models.Card.update(
    { blocked: false },
    { where: { id: params.value.card_id } },
  )
    .then(() => models.Card.findByPk(params.value.card_id))
    .then((card) => res.json(card))
    .catch(next);
};
