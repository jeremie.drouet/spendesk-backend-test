const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {get} /api/cards List all the user's cards
 * @apiName ListCard
 * @apiGroup Card
 *
 * @apiParams (query) user_id ID of the user
 *
 * @apiSuccess {String} id ID of the wallet
 * @apiSuccess {String} company_id id of the company related to the wallet
 * @apiSuccess {String=USD,GBP,EUR} currency currencu of the wallet
 * @apiSuccess {Number} balance number of cents on the wallet
 * @apiSuccess {Boolean} master if the wallet is the master wallet
 */

const querySchema = Joi.object()
  .keys({
    user_id: Joi.string()
      .uuid()
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const query = querySchema.validate(req.query, { stripUnknown: true });
  if (query.error) return next(query.error);
  return models.Card.findAll({
    where: query.value,
  })
    .then((wallets) => res.json(wallets))
    .catch(next);
};
