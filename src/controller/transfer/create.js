const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {post} /api/transfers Create a transfer
 * @apiName CreateTransfer
 * @apiGroup Transfer
 *
 * @apiParam {String} company_id ID of the company linked to the wallet
 * @apiParam {String=USD,GBP,EUR} currency Currency of the wallet
 *
 * @apiSuccess {String} id ID of the wallet
 * @apiSuccess {String} company_id id of the company related to the wallet
 * @apiSuccess {String=USD,GBP,EUR} currency currencu of the wallet
 * @apiSuccess {Number} balance number of cents on the wallet
 * @apiSuccess {Boolean} master if the wallet is the master wallet
 *
 * @apiError ValidationError request body didn't satisfy requirements
 */

const schema = Joi.object()
  .keys({
    amount: Joi.number()
      .positive()
      .required(),
    origin_id: Joi.string()
      .uuid()
      .required(),
    target_id: Joi.string()
      .uuid()
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const body = schema.validate(req.body, { stripUnknown: true });
  if (body.error) return next(body.error);
  return models.Transfer.betweenWallet(
    body.value.origin_id,
    body.value.target_id,
    body.value.amount,
  )
    .then((transfer) => res.json(transfer))
    .catch(next);
};
