const Joi = require('@hapi/joi');
const models = require('../../model');

/**
 * @api {post} /api/wallets Create a wallet
 * @apiName CreateWallet
 * @apiGroup Wallet
 *
 * @apiParam {String} company_id ID of the company linked to the wallet
 * @apiParam {String=USD,GBP,EUR} currency Currency of the wallet
 *
 * @apiSuccess {String} id ID of the wallet
 * @apiSuccess {String} company_id id of the company related to the wallet
 * @apiSuccess {String=USD,GBP,EUR} currency currencu of the wallet
 * @apiSuccess {Number} balance number of cents on the wallet
 * @apiSuccess {Boolean} master if the wallet is the master wallet
 *
 * @apiError ValidationError request body didn't satisfy requirements
 */

const schema = Joi.object()
  .keys({
    company_id: Joi.string()
      .uuid()
      .required(),
    currency: Joi.string()
      .uppercase()
      .valid('USD', 'GBP', 'EUR')
      .required(),
  })
  .required();

module.exports = (req, res, next) => {
  const body = schema.validate(req.body, { stripUnknown: true });
  if (body.error) return next(body.error);
  return models.Wallet.create(body.value)
    .then((wallet) => res.json(wallet))
    .catch(next);
};
