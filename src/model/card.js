const Boom = require('@hapi/boom');
const { addMonths } = require('date-fns');
const { createDigits } = require('../service/random');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Card',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
      },
      wallet_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      balance: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      number: {
        allowNull: false,
        type: DataTypes.TEXT,
        defaultValue: function() {
          return createDigits(16);
        },
      },
      cvv: {
        allowNull: false,
        type: DataTypes.TEXT,
        defaultValue: function() {
          return createDigits(3);
        },
      },
      expiration: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: function() {
          return addMonths(new Date(), 1);
        },
      },
      blocked: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      timestamps: false,
      tableName: 'cards',
    },
  );

  Model.associate = (db) => {
    // associations can be defined here
    Model.belongsTo(db.Wallet, {
      as: 'wallet',
      foreignKey: 'wallet_id',
    });
  };

  Model.scopes = (db) => {
    // scopes can be defined here
  };

  Model.block = (card_id) =>
    sequelize.transaction(async (transaction) => {
      const card = await Model.findByPk(card_id, { transaction });
      if (card.blocked) {
        throw Boom.preconditionFailed('card already blocked');
      }
      await Model.db.Transfer.unloadCard(card_id, card.balance, transaction);
      await card.update({ blocked: true }, { transaction });
    });

  return Model;
};
