module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Wallet',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
      },
      company_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      balance: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      master: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      timestamps: false,
      tableName: 'wallets',
    },
  );

  Model.associate = (db) => {
    // associations can be defined here
    Model.hasMany(db.Card, {
      as: 'cards',
      foreignKey: 'wallet_id',
    });
  };

  Model.scopes = (db) => {
    // scopes can be defined here
  };

  return Model;
};
