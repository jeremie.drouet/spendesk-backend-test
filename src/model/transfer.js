const Boom = require('@hapi/boom');
const Exchange = require('../service/exchange');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Transfer',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: sequelize.literal('uuid_generate_v4()'),
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
      },
      amount: {
        allowNull: false,
        // in cents
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      origin_currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      target_currency: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      conversion_fee: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      origin_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      origin_type: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      target_id: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      target_type: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
    },
    {
      timestamps: false,
      tableName: 'transfers',
    },
  );

  Model.associate = (db) => {
    // associations can be defined here
  };

  Model.scopes = (db) => {
    // scopes can be defined here
  };

  Model.loadCard = (card_id, amount) =>
    sequelize.transaction(async (transaction) => {
      const { Card } = Model.db;
      const target = await Card.findByPk(card_id, {
        transaction,
        include: ['wallet'],
      });
      if (target.blocked) {
        throw Boom.preconditionFailed('card blocked');
      }
      const origin = target.wallet;
      if (origin.balance < amount) {
        throw Boom.preconditionFailed('insufficient funds');
      }
      await origin.update(
        {
          balance: origin.balance - amount,
        },
        { transaction },
      );
      await target.update(
        {
          balance: target.balance + amount,
        },
        { transaction },
      );
      await Model.create(
        {
          amount,
          // SAME CURRENCY
          origin_currency: origin.currency,
          target_currency: target.currency,
          conversion_fee: 0,
          origin_id: origin.id,
          origin_type: 'wallet',
          target_id: target.id,
          target_type: 'card',
        },
        { transaction },
      );
    });

  Model.unloadCard = async (card_id, amount, transaction) => {
    if (!transaction) {
      return sequelize.transaction((tx) =>
        Model.unloadCard(card_id, amount, tx),
      );
    }
    const { Card, Wallet } = Model.db;
    const origin = await Card.findByPk(card_id, {
      transaction,
      include: ['wallet'],
    });
    const target = origin.wallet;
    if (origin.balance < amount) {
      throw Boom.preconditionFailed('insufficient funds');
    }
    await Card.update(
      {
        balance: origin.balance - amount,
      },
      { where: { id: card_id }, transaction },
    );
    await Wallet.update(
      {
        balance: target.balance + amount,
      },
      { where: { id: target.id }, transaction },
    );
    await Model.create(
      {
        amount,
        // SAME CURRENCY
        origin_currency: origin.currency,
        target_currency: target.currency,
        conversion_fee: 0.0,
        origin_id: origin.id,
        origin_type: 'card',
        target_id: target.id,
        target_type: 'wallet',
      },
      { transaction },
    );
  };

  Model.betweenWallet = (
    origin_id,
    target_id,
    amount /* in origin currency */,
  ) => {
    const { Wallet } = Model.db;
    return sequelize.transaction(async (transaction) => {
      const origin = await Wallet.findByPk(origin_id, { transaction });
      const target = await Wallet.findByPk(target_id, { transaction });
      if (origin.balance < amount) {
        throw Boom.preconditionFailed('insufficient funds');
      }
      const rate = await Exchange.getRate(origin.currency, target.currency);
      const target_amount = Math.floor(amount * rate);
      const conversionFeeRate = Exchange.getConversionFeeRate(
        origin.currency,
        target.currency,
      );
      const conversion_fee = Math.floor(target_amount * conversionFeeRate);
      await Wallet.update(
        {
          balance: target.balance + target_amount - conversion_fee,
        },
        { where: { id: target.id }, transaction },
      );
      await Wallet.update(
        {
          balance: origin.balance - amount,
        },
        { where: { id: origin.id }, transaction },
      );
      const transfer = await Model.create(
        {
          amount, // in origin currency
          origin_currency: origin.currency,
          target_currency: target.currency,
          conversion_fee,
          origin_id: origin.id,
          origin_type: 'wallet',
          target_id: target.id,
          target_type: 'wallet',
        },
        { transaction },
      );
      return transfer;
    });
  };

  return Model;
};
