module.exports = {
  port: process.env.PORT || 3000,
  database: {
    url: process.env.DATABASE_URL || 'postgres://postgres@localhost/postgres',
  },
  fixer: {
    accessKey: process.env.FIXER_ACCESS_KEY || '93674d960bc8b997d469c4b63c639236',
  },
};
